FROM python:3.8-alpine

RUN apk add --no-cache \
  curl \
  libffi-dev \
  openssl-dev \
  build-base &&\
  rm -rf /var/cache/apk/*

COPY requirements.txt .
RUN pip install -r requirements.txt &&\
  rm -f requirements.txt

COPY app.py /usr/local/bin/app

EXPOSE 5000

ENTRYPOINT ["app"]
